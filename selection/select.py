
from ROOT import *


pathPrefix = "/eos/user/v/vazhovko/Data_LstLambda/"
ids = [126,127,130]
maxSj = 300


cutID  = "ProtonP_ProbNNp>0.4 && ProtonM_ProbNNp>0.4 && KaonM_ProbNNk>0.4 && PiP_ProbNNpi>0.4"
cutVtx = "Jpsi_ENDVERTEX_CHI2<27 && Lambda_ENDVERTEX_CHI2<9"
cutFD  = "Lambda_FDCHI2_OWNPV>25"


ch = TChain("Jpsi2LstLambdaTuple/DecayTree")
for ID in ids:
	for isj in range(maxSj):
		path = pathPrefix + str(ID) + "/" + str(isj) + "/Tuple.root"
		ch.Add(path)

newfile = TFile("/eos/user/a/ausachov/Data_LstLambda/preselected.root","recreate")
tree = ch.CopyTree(cutID+" && "+cutVtx+" && "+cutFD)
tree.Print()
tree.GetCurrentFile().Write() 
tree.GetCurrentFile().Close()
