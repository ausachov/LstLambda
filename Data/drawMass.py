from ROOT import *

cutTrigger = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)"
cutTrack = "ProtonP_TRACK_CHI2NDOF<3 && ProtonM_TRACK_CHI2NDOF<3 && PiP_TRACK_CHI2NDOF<3 && KaonM_TRACK_CHI2NDOF<3"
cutVtx = "Lst_ENDVERTEX_CHI2<4 && Lambda_ENDVERTEX_CHI2<4 && Jpsi_ENDVERTEX_CHI2<20"
cutPID = "ProtonP_ProbNNp>0.0 && ProtonM_ProbNNp>0.0" #&& PiP_ProbNNk>0.1 && PiM_ProbNNk>0.1"
cutIP = "ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16 && PiP_IPCHI2_OWNPV>16 && KaonM_IPCHI2_OWNPV>16"
cutP = "ProtonP_P>10000 && ProtonM_P>10000"


# cutPhiVeto = "Phi_M>1030"
#cutPhiVeto = "abs(Phi_M-1020)>10 && abs(Lst1_p2K_M-1020)>10 && abs(Lst2_p2K_M-1020)>10 && abs(Etac_PpPm2KpKm_M-1020)>10"
#cutLstM = "Lst1_M>1500 && Lst2_M>1500 && Lst1_M<1540 && Lst2_M<1540"

cutDef = cutTrigger+" && "+cutTrack+" && "+cutVtx+" && "+cutPID+" && "+cutIP+" && "+cutP

ch = TChain("DecayTree")
ch.Add("LstLambda_RunII_merged.root")
h = TH1F("h","h",150,3000,4500)
h1 = TH1F("h1","h1",150,3000,4500)

#ch.Draw("Lst1_M",cut0+" && "+cutPT)
