import os
from ROOT import *

jobIDs = [641]

for jID in jobIDs:
   
   mainDir = '/eos/user/a/ausachov/DataLstLambda/'+str(jID)
   
   nSubJobs = 940
   for nSj in range(900,nSubJobs):
      sjDir = mainDir+'/'+str(nSj)
      try:
         chainFileName = sjDir+'/Tuple.root'
         ch = TChain("Jpsi2LstLambdaTuple/DecayTree")
         ch.Add(chainFileName)
         newfile = TFile(sjDir+"/triggeredTuple.root","recreate")
         tree = ch.CopyTree("(Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && \
         (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS)")
         tree.GetCurrentFile().Write() 
         tree.GetCurrentFile().Close()
      except:
         pass


