def fillTuple( tuple, myTriggerList ):
        
    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats"
                     ]
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')

    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

#    from Configurables import TupleToolDecayTreeFitter
#    tuple.Jpsi.ToolList +=  ["TupleToolDecayTreeFitter/PVFit"]        # fit with both PV and mass constraint
#    tuple.Jpsi.addTool(TupleToolDecayTreeFitter("PVFit"))
#    tuple.Jpsi.PVFit.Verbose = True
#    tuple.Jpsi.PVFit.constrainToOriginVertex = True
#    tuple.Jpsi.PVFit.daughtersToConstrain = []


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    tuple.addTool(TupleToolDecay, name = 'Lst')
    tuple.Lst.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForLst" ]
    tuple.Lst.addTool(TupleToolTISTOS, name="TupleToolTISTOSForLst" )
    tuple.Lst.TupleToolTISTOSForLst.Verbose=True
    tuple.Lst.TupleToolTISTOSForLst.TriggerList = myTriggerList

    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Lst=LoKi__Hybrid__TupleTool("LoKi_Lst")
    LoKi_Lst.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"           : "DTF_FUN ( M , False )",
      "DOCA"               : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }    
    tuple.Lst.addTool(LoKi_Lst)





    tuple.addTool(TupleToolDecay, name = 'Lambda')
    tuple.Lambda.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForLambda" ]
    tuple.Lambda.addTool(TupleToolTISTOS, name="TupleToolTISTOSForLambda" )
    tuple.Lambda.TupleToolTISTOSForLambda.Verbose=True
    tuple.Lambda.TupleToolTISTOSForLambda.TriggerList = myTriggerList

    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Lambda=LoKi__Hybrid__TupleTool("LoKi_Lambda")
    LoKi_Lambda.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )",
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Lambda.addTool(LoKi_Lambda)



    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTool(LoKi_All)
    
myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
                 
                 
                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",
                 
                 "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMVALooseDecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TwoTrackMVALooseDecision"
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1CalibTrackingKKDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",
                
                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptLambdaEETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 
                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
    ]







year = "2017"
Jpsi2LstLambdaDecay = "[J/psi(1S) -> ^( Lambda(1520)0 -> ^p+ ^K-) ^( Lambda~0  -> ^p~- ^pi+)]CC"
Jpsi2LstLambdaBranches = {
     "KaonM"   :  "[J/psi(1S) -> ( Lambda(1520)0 -> p+ ^K-) ( Lambda~0  -> p~- pi+)]CC"
    ,"PiP"     :  "[J/psi(1S) -> ( Lambda(1520)0 -> p+ K-) ( Lambda~0  -> p~- ^pi+)]CC"
    ,"ProtonP" :  "[J/psi(1S) -> ( Lambda(1520)0 -> ^p+ K-) ( Lambda~0  -> p~- pi+)]CC"
    ,"ProtonM" :  "[J/psi(1S) -> ( Lambda(1520)0 -> p+ K-) ( Lambda~0  -> ^p~- pi+)]CC"
    ,"Lst"     :  "[J/psi(1S) -> ^( Lambda(1520)0 -> p+ K-) ( Lambda~0  -> p~- pi+)]CC"
    ,"Lambda"  :  "[J/psi(1S) -> ( Lambda(1520)0 -> p+ K-) ^( Lambda~0  -> p~- pi+)]CC"
    ,"Jpsi"    :  "[J/psi(1S) -> ( Lambda(1520)0 -> p+ K-) ( Lambda~0  -> p~- pi+)]CC"
    }

Jpsi2LstLambdaLocation = "Phys/Ccbar2LstLambdaLine/Particles"
from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
inputData = AutomaticData(Jpsi2LstLambdaLocation) 
inputData = MomentumScaling(inputData, Year = year)

Jpsi2LstLambdaTuple = TupleSelection("Jpsi2LstLambdaTuple", inputData, Decay = Jpsi2LstLambdaDecay, Branches = Jpsi2LstLambdaBranches)
fillTuple( Jpsi2LstLambdaTuple, myTriggerList )

from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Ccbar2LstLambdaFilters = LoKi_Filters (
    STRIP_Code = """HLT_PASS('StrippingCcbar2LstLambdaLineDecision')"""
    )



from Configurables import DaVinci, CondDB
CondDB ( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Ccbar2LstLambdaFilters.filters('Ccbar2LstLambdaFilters')
DaVinci().EvtMax = -1                         # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ eventNodeKiller,
                            Jpsi2LstLambdaTuple ]        # The algorithms
# MDST
DaVinci().InputType = "MDST"
DaVinci().RootInTES = "/Event/Charm"

DaVinci().Lumi = True

DaVinci().DDDBtag   = "dddb-20170721-3"
#DaVinci().CondDBtag = "cond-20161004"


from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

#from GaudiConf import IOHelper
### Use the local input data
#IOHelper().inputFiles([
#                       '00071501_00000046_1.charm.mdst'
#                       ], clear=True)
