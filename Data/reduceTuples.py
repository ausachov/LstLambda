import os
from ROOT import *

jobsN = [123,124,128,130]
nSubJobs = 300


reducingCut = "Lambda_ENDVERTEX_CHI2<9 && Lst_ENDVERTEX_CHI2<9 && Jpsi_ENDVERTEX_CHI2<45 && ProtonP_PT>250 && KaonM_PT>250 && ProtonM_PT>250 && PiP_PT>250 && Lambda_FDCHI2_OWNPV>25 && PiP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && ProtonP_IPCHI2_OWNPV>9 && KaonM_IPCHI2_OWNPV>9 && Jpsi_FDCHI2_OWNPV>25"


for iJ in jobsN:
   mainDir = '/eos/lhcb/wg/BandQ/PRODPOL/LstLambda/Data/'+str(iJ)
   for nSj in range(nSubJobs):
      sjDir = mainDir+'/'+str(nSj)
      try:
         chainFileName = sjDir+'/Tuple.root'
         ch = TChain("Jpsi2LstLambdaTuple/DecayTree")
         ch.Add(chainFileName)
         newfile = TFile(sjDir+"/reducedTuple.root","recreate")
         tree = ch.CopyTree(reducingCut)
         tree.GetCurrentFile().Write() 
         tree.GetCurrentFile().Close()
      except:
         pass
      print nSj, "processed"

